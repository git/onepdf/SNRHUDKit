//
//  SNRHUDHorizontalSeperator.m
//  SNRHUDKit
//
//  Created by Simon on 12/02/13.
//  Copyright (c) 2014 one-pdf.com. All rights reserved.
//

#import "SNRHUDHorizontalSeperator.h"

#define SEPERATOR_COLORS  [NSArray arrayWithObjects:\
[NSColor colorWithDeviceWhite:1.00 alpha:0.00],\
[NSColor colorWithDeviceWhite:1.00 alpha:0.70],\
[NSColor colorWithDeviceWhite:1.00 alpha:0.70],\
[NSColor colorWithDeviceWhite:1.00 alpha:0.70],\
[NSColor colorWithDeviceWhite:1.00 alpha:0.00],\
nil]

@implementation SNRHUDHorizontalSeperator

- (void)drawRect:(NSRect)dirtyRect {
    NSRect drawingRect = self.bounds;
    drawingRect.size.height = 1;
    drawingRect.origin.y += 2;
    NSGradient *gradient = [[NSGradient alloc] initWithColors:SEPERATOR_COLORS];
    [gradient drawInRect:drawingRect angle:0];
}

@end
