//
//  SNRHUDButtonCell.h
//  SNRHUDKit
//
//  Created by Indragie Karunaratne on 12-01-23.
//  Copyright (c) 2012 indragie.com. All rights reserved.
//

#import <AppKit/AppKit.h>

// TODO: Drawing images

@interface SNRHUDButtonCell : NSButtonCell

@property (nonatomic) BOOL isBlue;

@end
