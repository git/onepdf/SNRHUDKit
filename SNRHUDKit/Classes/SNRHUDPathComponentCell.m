//
//  SNRHUDPathComponentCell.m
//  SNRHUDKit
//
//  Created by Simon on 12/02/13.
//  Copyright (c) 2014 one-pdf.com. All rights reserved.
//

#import "SNRHUDPathComponentCell.h"

@implementation SNRHUDPathComponentCell

- (id)init {
    if ((self = [super init])) {
        [self setTextColor:[NSColor whiteColor]];
    }
    return self;
}

- (void)awakeFromNib {
    [self setTextColor:[NSColor whiteColor]];
}

@end
