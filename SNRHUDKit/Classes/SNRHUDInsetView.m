//
//  SNRHUDInsetView.m
//  SNRHUDKit
//
//  Created by Simon on 8/01/13.
//  Copyright (c) 2014 one-pdf.com. All rights reserved.
//

#import "SNRHUDInsetView.h"
#import "NSBezierPath+MCAdditions.h"

#define SNRInsetViewBackgroundColor             [NSColor colorWithDeviceWhite:0.000 alpha:0.150]

#define SNRInsetViewInnerGlowColor              [NSColor colorWithDeviceWhite:0.000 alpha:0.750]
#define SNRInsetViewInnerGlowOffset             NSMakeSize(0.f, 0.f)
#define SNRInsetViewInnerGlowBlurRadius         10.f

@implementation SNRHUDInsetView

- (void)drawRect:(NSRect)dirtyRect {
    NSRect backgroundRect = self.bounds;
    backgroundRect.size.width   += 20;
    backgroundRect.origin.x     -= 20;
    
    NSBezierPath *backgroundPath = [NSBezierPath bezierPathWithRect:backgroundRect];
    
    [SNRInsetViewBackgroundColor set];
    [backgroundPath fill];
    
    NSShadow *innerGlow = [NSShadow new];
    [innerGlow setShadowColor:SNRInsetViewInnerGlowColor];
    [innerGlow setShadowOffset:SNRInsetViewInnerGlowOffset];
    [innerGlow setShadowBlurRadius:SNRInsetViewInnerGlowBlurRadius];
    [backgroundPath fillWithInnerShadow:innerGlow];
    
    [super drawRect:dirtyRect];
}

@end
