//
//  SNRHUDOutlineView.m
//  SNRHUDKit
//
//  Created by Simon on 8/01/13.
//  Copyright (c) 2014 one-pdf.com. All rights reserved.
//

#import "SNRHUDOutlineView.h"
#import "NSBezierPath+MCAdditions.h"

#define SNROutlineViewSelectionColors             [NSArray arrayWithObjects:\
[NSColor colorWithDeviceWhite:0.150 alpha:1.000],\
[NSColor colorWithDeviceWhite:0.200 alpha:1.000],\
[NSColor colorWithDeviceWhite:0.220 alpha:1.000],\
[NSColor colorWithDeviceWhite:0.200 alpha:1.000],\
[NSColor colorWithDeviceWhite:0.150 alpha:1.000],\
nil]

@implementation SNRHUDOutlineView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
        self.backgroundColor = [NSColor clearColor];
        //[[self enclosingScrollView] setDrawsBackground:NO];
    }
    
    return self;
}

// Private stuff (makes dragging and dropping look SO much better)

- (void)_flashOutlineCell { return; }
- (void)_highlightOutlineCell:(NSCell*)cell highlight:(BOOL)highlight withFrame:(CGRect)frame inView:(id)arg4 { return; }

- (void)_drawDropHighlightBackgroundForRow:(NSInteger)row {
    [NSGraphicsContext saveGraphicsState];
    NSRect rowRect = [self rectOfRow:row];
    
    NSRect insetRowRect = CGRectInset(rowRect, 2, 2);
    NSBezierPath *drawPath = [NSBezierPath bezierPathWithRoundedRect:insetRowRect xRadius:6 yRadius:6];
    
    [[NSColor darkGrayColor] set];
    [drawPath setLineWidth:2];
    [drawPath stroke];
    [[NSColor colorWithCalibratedWhite:1.0 alpha:0.12] set];
    [drawPath fill];
    
    [NSGraphicsContext restoreGraphicsState];
}

+ (NSColor*)_dropHighlightColor {
    return [NSColor darkGrayColor];
}
+ (NSColor*)_dropHighlightBackgroundColor {
    return [NSColor colorWithCalibratedWhite:1.0 alpha:0.12];
}
- (NSColor*)_dropHighlightColor {
    return [NSColor darkGrayColor];
}
- (NSColor*)_dropHighlightColorForRow:(NSInteger)row {
    return [NSColor darkGrayColor];
}
- (NSColor*)_dropHighlightBackgroundColor {
    return [NSColor colorWithCalibratedWhite:1.0 alpha:0.12];
}
- (NSColor*)_dropHighlightColorForEntireTableView {
    return [NSColor darkGrayColor];
}

- (void)highlightSelectionInClipRect:(NSRect)clipRect {
    NSIndexSet *selectedRowIndexes = [self selectedRowIndexes];
    NSRange visibleRows = [self rowsInRect:clipRect];
    
    NSUInteger currentRow = [selectedRowIndexes firstIndex];
    while (currentRow != NSNotFound) {
        if (currentRow == -1 || !NSLocationInRange(currentRow, visibleRows)) {
            currentRow = [selectedRowIndexes indexGreaterThanIndex:currentRow];
            continue;
        }
        
        NSRect aRowRect = [self rectOfRow:currentRow];
        aRowRect.size.height--;
        // This stops the selection from overlapping the border...
        aRowRect.size.width-=2;
        aRowRect.origin.x++;
        
        [NSGraphicsContext saveGraphicsState];
        [[NSGraphicsContext currentContext] setCompositingOperation:NSCompositeCopy];
        
        NSGradient *gradient = [[NSGradient alloc] initWithColors:SNROutlineViewSelectionColors];
        [gradient drawInRect:aRowRect angle:0];
        
        [NSGraphicsContext restoreGraphicsState];
        
        currentRow = [selectedRowIndexes indexGreaterThanIndex:currentRow];
    }
}

- (void)drawRow:(NSInteger)row clipRect:(NSRect)clipRect {
    CGContextSetAlpha([[NSGraphicsContext currentContext] graphicsPort], 1.0f);
    [super drawRow:row clipRect:clipRect];
}

@end
