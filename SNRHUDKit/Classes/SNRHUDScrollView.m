//
//  SNRHUDScrollView.m
//  SNRHUDKit
//
//  Created by Indragie Karunaratne on 12-01-23.
//  Copyright (c) 2012 indragie.com. All rights reserved.
//

#import "SNRHUDScrollView.h"
#import "NSBezierPath+MCAdditions.h"

@implementation SNRHUDScrollView

#define SNROutlineViewBackgroundColor             [NSColor colorWithDeviceWhite:0.000 alpha:0.150]
#define SNROutlineViewInnerGlowColor              [NSColor colorWithDeviceWhite:0.000 alpha:0.300]
#define SNROutlineViewInnerGlowOffset             NSMakeSize(0.f, 0.f)
#define SNROutlineViewInnerGlowBlurRadius         3.f

#define SNROutlineViewInnerShadowColor            [NSColor colorWithDeviceWhite:0.000 alpha:0.400]
#define SNROutlineViewInnerShadowOffset           NSMakeSize(0.f, -1.f)
#define SNROutlineViewInnerShadowBlurRadius       3.f

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        [self setScrollerKnobStyle:NSScrollerKnobStyleLight];
        [self setBackgroundColor:[NSColor clearColor]];
        [self setDrawsBackground:YES];
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect {
    NSRect backgroundRect = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height+1);
    
    NSBezierPath *backgroundPath = [NSBezierPath bezierPathWithRect:backgroundRect];
    
    NSShadow *innerGlow = [NSShadow new];
    [innerGlow setShadowColor:SNROutlineViewInnerGlowColor];
    [innerGlow setShadowOffset:SNROutlineViewInnerGlowOffset];
    [innerGlow setShadowBlurRadius:SNROutlineViewInnerGlowBlurRadius];
    [backgroundPath fillWithInnerShadow:innerGlow];
    NSRect innerShadowRect = NSInsetRect(backgroundRect, -2.f, 0.f);
    innerShadowRect.size.height *= 2.f;
    NSBezierPath *shadowPath = [NSBezierPath bezierPathWithRect:innerShadowRect];
    NSShadow *innerShadow = [NSShadow new];
    [innerShadow setShadowColor:SNROutlineViewInnerShadowColor];
    [innerShadow setShadowOffset:SNROutlineViewInnerShadowOffset];
    [innerShadow setShadowBlurRadius:SNROutlineViewInnerShadowBlurRadius];
    [shadowPath fillWithInnerShadow:innerShadow];
    
    [super drawRect:dirtyRect];
}

@end
