//
//  SNRHUDPathCell.h
//  SNRHUDKit
//
//  Created by Simon on 12/02/13.
//  Copyright (c) 2014 one-pdf.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SNRHUDPathComponentCell.h"

@interface SNRHUDPathCell : NSPathCell

@end
