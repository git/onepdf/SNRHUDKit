//
//  SNRHUDPathCell.m
//  SNRHUDKit
//
//  Created by Simon on 12/02/13.
//  Copyright (c) 2014 one-pdf.com. All rights reserved.
//

#import "SNRHUDPathCell.h"

#define SNRButtonBlackGradientBottomColor         [NSColor colorWithDeviceWhite:0.150 alpha:1.000]
#define SNRButtonBlackGradientTopColor            [NSColor colorWithDeviceWhite:0.220 alpha:1.000]
#define SNRButtonBlackHighlightColor              [NSColor colorWithDeviceWhite:1.000 alpha:0.050]

#define SNRButtonDisabledAlpha                    0.7f
#define SNRButtonCornerRadius                     3.f
#define SNRButtonDropShadowColor                  [NSColor colorWithDeviceWhite:1.000 alpha:0.050]
#define SNRButtonDropShadowBlurRadius             1.f
#define SNRButtonDropShadowOffset                 NSMakeSize(0.f, -1.f)
#define SNRButtonBlackTextShadowOffset            NSMakeSize(0.f, 1.f)
#define SNRButtonBlackTextShadowBlurRadius        1.f
#define SNRButtonBlackTextShadowColor             [NSColor blackColor]
#define SNRButtonBorderColor                      [NSColor blackColor]
#define SNRButtonHighlightOverlayColor            [NSColor colorWithDeviceWhite:0.000 alpha:0.300]

#define SNRButtonCheckboxCheckmarkColor           [NSColor colorWithDeviceWhite:0.820 alpha:1.000]
#define SNRButtonCheckboxCheckmarkShadowColor     [NSColor colorWithDeviceWhite:0.000 alpha:0.750]

@implementation SNRHUDPathCell

- (void)drawWithFrame:(NSRect)frame inView:(NSView *)controlView {
    if (self.pathStyle == NSPathStylePopUp) {
        frame = NSInsetRect(frame, 0.5f, 0.5f);
        frame.size.height -= SNRButtonDropShadowBlurRadius;
        NSBezierPath *bezelPath = [NSBezierPath bezierPathWithRoundedRect:frame xRadius:SNRButtonCornerRadius yRadius:SNRButtonCornerRadius];
        NSGradient *gradientFill = [[NSGradient alloc] initWithStartingColor:SNRButtonBlackGradientBottomColor endingColor:SNRButtonBlackGradientTopColor];
        // Draw the gradient fill
        [gradientFill drawInBezierPath:bezelPath angle:270.f];
        // Draw the border and drop shadow
        [NSGraphicsContext saveGraphicsState];
        [SNRButtonBorderColor set];
        NSShadow *dropShadow = [NSShadow new];
        [dropShadow setShadowColor:SNRButtonDropShadowColor];
        [dropShadow setShadowBlurRadius:SNRButtonDropShadowBlurRadius];
        [dropShadow setShadowOffset:SNRButtonDropShadowOffset];
        [dropShadow set];
        [bezelPath stroke];
        [NSGraphicsContext restoreGraphicsState];
        // Draw the highlight line around the top edge of the pill
        // Outset the width of the rectangle by 0.5px so that the highlight "bleeds" around the rounded corners
        // Outset the height by 1px so that the line is drawn right below the border
        NSRect highlightRect = NSInsetRect(frame, -0.5f, 1.f);
        // Make the height of the highlight rect something bigger than the bounds so that it won't show up on the bottom
        highlightRect.size.height *= 2.f;
        [NSGraphicsContext saveGraphicsState];
        NSBezierPath *highlightPath = [NSBezierPath bezierPathWithRoundedRect:highlightRect xRadius:SNRButtonCornerRadius yRadius:SNRButtonCornerRadius];
        [bezelPath addClip];
        [SNRButtonBlackHighlightColor set];
        [highlightPath stroke];
        [NSGraphicsContext restoreGraphicsState];
        
        NSRect popupFrame = NSMakeRect(NSMaxX(frame)- 12.f, NSMidY(frame) - (12.f/2.f), 7.5f, 12.f);
        
        NSShadow *textShadow = [NSShadow new];
        [textShadow setShadowOffset:SNRButtonBlackTextShadowOffset];
        [textShadow setShadowColor:SNRButtonBlackTextShadowColor];
        [textShadow setShadowBlurRadius:SNRButtonBlackTextShadowBlurRadius];
        [textShadow set];
        
        NSBezierPath* bezierPath = [NSBezierPath bezierPath];
        [bezierPath moveToPoint: NSMakePoint(NSMinX(popupFrame) + 0.75, NSMinY(popupFrame) + 4.5)];
        [bezierPath lineToPoint: NSMakePoint(NSMinX(popupFrame) + 3.75, NSMinY(popupFrame))];
        [bezierPath lineToPoint: NSMakePoint(NSMinX(popupFrame) + 6.75, NSMinY(popupFrame) + 4.5)];
        [bezierPath lineToPoint: NSMakePoint(NSMinX(popupFrame) + 0.75, NSMinY(popupFrame) + 4.5)];
        [bezierPath closePath];
        [SNRButtonCheckboxCheckmarkColor setFill];
        [bezierPath fill];
        
        NSBezierPath* bezier3Path = [NSBezierPath bezierPath];
        [bezier3Path moveToPoint: NSMakePoint(NSMinX(popupFrame) + 0.75, NSMinY(popupFrame) + 7.5)];
        [bezier3Path lineToPoint: NSMakePoint(NSMinX(popupFrame) + 3.75, NSMinY(popupFrame) + 12)];
        [bezier3Path lineToPoint: NSMakePoint(NSMinX(popupFrame) + 6.75, NSMinY(popupFrame) + 7.5)];
        [bezier3Path lineToPoint: NSMakePoint(NSMinX(popupFrame) + 0.75, NSMinY(popupFrame) + 7.5)];
        [bezier3Path closePath];
        [SNRButtonCheckboxCheckmarkColor setFill];
        [bezier3Path fill];
        
        [self drawInteriorWithFrame: CGRectMake(frame.origin.x-4, frame.origin.y, frame.size.width+30, frame.size.height) inView:controlView];
    } else {
        [super drawWithFrame:frame inView:controlView];
    }
}

- (NSRect)rectOfPathComponentCell:(NSPathComponentCell *)cell withFrame:(NSRect)frame inView:(NSView *)view {
    return CGRectOffset(frame,-4,0);
}

+ (Class)pathComponentCellClass {
    return [SNRHUDPathComponentCell class];
}

@end
