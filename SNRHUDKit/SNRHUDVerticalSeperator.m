//
//  SNRHUDVerticalSeperator.m
//  SNRHUDKit
//
//  Created by Simon on 12/02/13.
//  Copyright (c) 2014 one-pdf.com. All rights reserved.
//

#import "SNRHUDVerticalSeperator.h"

#define SEPERATOR_COLORS  [NSArray arrayWithObjects:\
[NSColor colorWithDeviceWhite:1.00 alpha:0.00],\
[NSColor colorWithDeviceWhite:1.00 alpha:0.70],\
[NSColor colorWithDeviceWhite:1.00 alpha:0.70],\
[NSColor colorWithDeviceWhite:1.00 alpha:0.70],\
[NSColor colorWithDeviceWhite:1.00 alpha:0.00],\
nil]

@implementation SNRHUDVerticalSeperator

- (void)drawRect:(NSRect)dirtyRect {
    NSRect drawingRect = self.bounds;
    drawingRect.size.width = 1;
    drawingRect.origin.x += 2;
    NSGradient *gradient = [[NSGradient alloc] initWithColors:SEPERATOR_COLORS];
    [gradient drawInRect:self.bounds angle:90];
}

@end
